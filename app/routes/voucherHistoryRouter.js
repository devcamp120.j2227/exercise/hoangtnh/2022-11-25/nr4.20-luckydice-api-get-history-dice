const express = require("express");
const router = express.Router();
const voucherHisController = require("../controllers/voucherHistoryController");

router.post("/voucherHistories", voucherHisController.createVoucherHistory);
router.get("/voucherHistories", voucherHisController.getAllVoucherHistory);
router.get("/voucherHistories/:voucherHisId", voucherHisController.getVoucherHistoryById);
router.put("/voucherHistories/:voucherHisId", voucherHisController.updateVoucherHistoryById);
router.delete("/voucherHistories/:voucherHisId", voucherHisController.deleteVoucherHistoryById)
module.exports = router;