//khai báo thư viện mongoose
const mongoose = require("mongoose");

// import prize model
const prizeModel = require("../models/prizeModel");

//function create prize
const createPrize = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    //B2: validate dữ liệu
    if(!body.name){
        return response.status(400).json({
            status: "Bad Request",
            message:"name không hợp lệ"
        })
    }
    
    //B3: gọi model tạo dữ liệu
    const newPrize = {
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description,
        createAt: body.createAt,
        updateAt: body.updatedAt
    }
    if(body.name !== undefined){
        newPrize.name = body.name
    }
    if(body.description !== undefined){
        newPrize.description = body.description
    }
    prizeModel.create(newPrize, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new prize successfully",
            data: data
        })
    })
}
//function get all prizes
const getAllPrizes = (request, response) => {
    prizeModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all prizes successfully",
            data: data
        })
    })
}
//function get prize by id
const getPrizeById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeId = request.params.prizeId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return response.status(400).json({
            status:"Bad Request",
            message: "prize id không hợp lệ"
        })
    }
    //B3: gọi model chưa id prize 
    prizeModel.findById(prizeId,(error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get prize by id ${prizeId} successfully`,
            data: data
        })
    })
}
//function update prize by id
const updatePrizeById = (request, response) => {
    //B1: chuẩn bị dữ liệu'
    const prizeId = request.params.prizeId;
    const body = request.body;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return response.status(400).json({
            status: "Bad request",
            message: "Prize Id không đúng"
        })
    }
    if(!body.name){
        return response.status(400).json({
            status: "Bad Request",
            message:"Prize name không hợp lệ"
        })
    }
    
    //B3: gọi model chưa id để tìm và update dữ liệu
    const updatePrize = {};
    if(body.name !== undefined){
        updatePrize.name = body.name
    }
    if(body.description !== undefined){
        updatePrize.description = body.description
    }
    prizeModel.findByIdAndUpdate(prizeId, updatePrize, (error, data)=>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Update Prize successfully",
            data: data
        })
    })
}

//function delete prize by id
const deletePrizeById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const prizeId = request.params.prizeId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeId)){
        return response.status(400).json({
            status:" Bad request",
            message:"Prize id không đúng"
        })
    }
    //B3: gọi prize model chưa id cần xóa
    prizeModel.findByIdAndRemove(prizeId,(error, data) => {
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`delete Prize had id ${prizeId} successfully`
        })
    })
}
module.exports = {
    createPrize,
    getAllPrizes,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}